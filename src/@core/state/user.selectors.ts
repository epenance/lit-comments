import {State} from "./reducer";

export const isLoggedIn = (state: State) => state.user.loggedIn;