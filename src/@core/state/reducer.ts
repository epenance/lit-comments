import { createStore, combineReducers } from 'redux';
import {commentsReducer, CommentsState} from "./comments.reducer";
import {userReducer, UserState} from "./user.reducer";

export interface State {
    comments: CommentsState,
    user: UserState
};

export const store = createStore(combineReducers({comments: commentsReducer, user: userReducer}), (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());