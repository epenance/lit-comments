import {State} from "./reducer";

export const lastComment = (state: State) => state.comments.comments[state.comments.comments.length - 1];