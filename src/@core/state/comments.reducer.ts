export interface CommentsState {
    comments: any[]
}

export const INITIAL_STATE: CommentsState = {
    comments: [],
};

export const commentsReducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
      case 'ADD_COMMENT':
          console.log(action);
          return {...state, comments: [...state.comments, action.payload]};

      default:
          return state;
  }
};