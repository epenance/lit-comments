

export interface UserState {
    loggedIn: boolean,
    loggingIn: boolean,
    userId: string
}

const INITIAL_STATE: UserState = {
    loggedIn: false,
    loggingIn: false,
    userId: ''
};

export const userReducer = (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case 'LOGGED_IN_SUCCESS':
            return {...state, loggedIn: true, loggingIn: false};

        case 'LOGGED_IN_FAILED':
            return {...state, loggedIn: false, loggingIn: false};

        case 'LOG_IN':
            return {...state, loggingIn: true};

        default:
            return state;
    }
};