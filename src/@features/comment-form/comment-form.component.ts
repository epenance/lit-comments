import {customElement, html, LitElement} from "lit-element";
import {connect} from "pwa-helpers/connect-mixin";
import {State, store} from '../../@core/state/reducer';
import {lastComment} from "../../@core/state/comments.selectors";

@customElement('comment-form')
export default class CommentForm extends connect(store)(LitElement) {

    loggedIn = false;
    loggingIn = false;
    latestComment: string | undefined;

    inputValue = '';

    logIn() {
        store.dispatch({type: 'LOG_IN'});
    }

    stateChanged(state: State) {

        this.loggingIn = state.user.loggingIn;

        this.latestComment = lastComment(state);
        this.requestUpdate();
    }

    sendComment() {
        console.log(this.inputValue);
        store.dispatch({ type: 'ADD_COMMENT', payload: this.inputValue || 'Default' });
        this.inputValue = '';
        this.requestUpdate();
    }

    render() {
        return html`<form>
                        <h1>${this.latestComment}</h1>
                        <textarea @change=${(e: any) => this.inputValue = e.target.value} placeholder="Your message..."></textarea>
                        <button type="button" @click="${() => this.sendComment()}">Send comment</button>
                        <button type="button" class="button" @click="${this.logIn}">Login</button>
                    </form>`
    }
}