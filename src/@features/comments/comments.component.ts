import { LitElement, html, property, customElement } from 'lit-element';
import * as socketIo from 'socket.io-client';
import {repeat} from "lit-html/directives/repeat";
import axios from 'axios';
import {connect} from "pwa-helpers/connect-mixin";
import {State, store} from "../../@core/state/reducer";

@customElement('simple-greeting')
export default class SimpleGreeting extends connect(store)(LitElement) {
    @property() name = 'World';

    comments: any[] = [];

    socket = socketIo('http://localhost:3000');

    constructor() {
        super();

        this.listenForAuth();
    }

    listenForAuth() {
        this.socket.on('event', (res: any) => {
            this.comments.push(res.message);

            this.requestUpdate();

        })
    }

    stateChanged(state: State) {
        this.comments = state.comments.comments;

        this.requestUpdate();
    }

    clickHandler() {
        console.log('Clicked');
        axios.get('http://localhost:3000/hello');

        this.name = 'Hello'
    }

    render() {
        return html`<p>
                        Hello, ${this.name}!
                    </p>
                    <comment-form></comment-form>
                     <ul>
                        ${repeat(this.comments, (comment) => html`<li>${comment}</li>`)}
                     </ul>
                     <button @click="${this.clickHandler}" type="button">Click me</button>`;
    }
};