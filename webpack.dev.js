const webpack = require('webpack');
const merge = require('webpack-merge');

const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist'
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    performance: {
        maxEntrypointSize: 400000000,
        maxAssetSize: 400000000,
    }
});